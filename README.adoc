= Sharded-Volumes example
Kumori Systems v1.0.8, Feb 2023
:compat-mode:
:toc:
:icons: font
:toc-title: Index
:toclevels: 3
:doctype: article
:experimental:
:icons: font
:sectanchors:
:sectlinks:
:sectnums:
:imagesdir: ./images

This example deploys a Kumori Service Application composed of two components:

* A Frontend component exposing a RestAPI to get/add/delete key-value pairs
* A Worker component running LevelDB database backend, using a persistent volume
  resource

The repository contains both the manifests needed to deploy the service within
a Kumori Platorm, and the source code (Node.js) used to build the docker images
of the two components. +
Docker images are available in the community repositories.

== A simple and limited sharded database

The service implements a sharded key-value database: depending on the key,
it is stored in one shard or other.

It is a very simple and limited implementation: it is assumed that the number
of shards is fixed:

* A *fixed* number of shards (Worker replicas) must be specified at the time of
  deployment. This initial number of shards can be modified (if is modified, an
  unexpected behaviour will occur).
* If the service is undeployed and deployed again using the same persistent
  volume resource, the *same* number of shards must be used.
* The number of replicas of the worker component cannot be increases. More
  specifically: the added replicas will never be used.
* If the number of replicas of the Worker component is reduced, the key-to-replica
  allocation algorithm will continue to try to use replicas that no longer exist.

== How the service works

The Frontend component exposes a RestAPI to get/add/delete key-value pairs, and
calculates the Worker replica to be used to store that key-value.

Worker identity is stored in a special key named "__ID__" in each LevelDB shard.
When a Worker instance is created, it checks if the "__ID__" key already exists
in its database; if it does not exist, the key is created with a value based in
the name of the instance.

Frontend and Worker components use a client->FULL->server link: the Frontend
is aware of which Workers exist, and can communicate with them individually.

image:deployment.png[frontend]

== Frontend RestAPI

* GET    https://<service-url>/keys/<key-name>
* POST   https://<service-url>/keys/<key-name>/<key-value>
* DELETE https://<service-url>/keys/<key-name>

== Key-to-replica allocation algorithm

How does the component end to which shard it should send a certain key?

```
// Calculates the sum of the ascii values of all characters in the key,
// and calculates the modulus with respect to the number of shards
for (let i = 0; i < key.length; i++) {
  value = value + key.charCodeAt(i)
}
shard = value % SHARD_SIZE
```

== How deploy the service

See the `util.sh` script.