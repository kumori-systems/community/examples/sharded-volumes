package service

import (
  k "kumori.systems/kumori:kumori"
  f ".../frontend:component"
  w ".../worker:component"
)

#Artifact: {
  ref: name:  "service"

  description: {

    //
    // Kumori Component roles and configuration
    //

    config: {
      parameter: {
        shardSize: int
      }
      resource: {
        store: k.#Volume
      }
    }

    role: {
      frontend: {
        artifact: f.#Artifact
        config: {
          parameter: {
            shardSize: description.config.parameter.shardSize
          }
          resource: {}
          resilience: description.config.resilience
        }
      }

      worker: {
        artifact: w.#Artifact
        config: {
          parameter: {
            shardSize: description.config.parameter.shardSize
          }
          resource: {
            store: description.config.resource.store
          }
          resilience: description.config.resilience
        }
      }
    }

    //
    // Kumori Service topology: how roles are interconnected
    //

    srv: {
      server: {
        restapi: { protocol: "http", port: 80 }
      }
    }

    connect: {
      // Outside -> FrontEnd (LB connector)
      cinbound: {
        as: "lb"
  			from: self: "restapi"
        to: frontend: "restapi": _
      }
      // FrontEnd -> Worker (LB connector)
      cbddconnector: {
        as: "full"
        from: frontend: "bddclient"
        to: worker: "bddserver": _
      }
    }
  }
}
