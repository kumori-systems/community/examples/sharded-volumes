package deployment

import s ".../service:service"

#Deployment: {
  name: "shardedvolumesdep"
  artifact: s.#Artifact
  config: {
    parameter: {
      shardSize: 3
    }
    resource: {
      store: volume: "myshardedvolume"
      // If we want use a volatile volume instead of persistent volumes:
      // store: volume: { size: 1, unit: "G" } // if volatile volume is used
    }
    scale: detail: frontend: hsize: 1
    scale: detail: worker: hsize: config.parameter.shardSize
    resilience: 0
  }
}
