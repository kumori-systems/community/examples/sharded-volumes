package component

import k "kumori.systems/kumori:kumori"

#Artifact: {
  ref: name:  "worker"

  description: {

    srv: {
      server: {
        bddserver: { protocol: "http", port: 8080 }
      }
    }

    config: {
      parameter: {
        shardSize: int
        store: "/usr/src/app/data"
      }
      resource: {
        store: k.#Volume
      }
    }

    size: {
      bandwidth: { size: 10, unit: "M" }
    }

    probe: worker: {
      liveness: {
        protocol: http : { port: srv.server.bddserver.port, path: "/health" }
        startupGraceWindow: { unit: "ms", duration: 120000, probe: true }
        frequency: "medium"
        timeout: 30000  // msec
      }
      readiness: {
        protocol: http : { port: srv.server.bddserver.port, path: "/health" }
        frequency: "medium"
        timeout: 30000  // msec
      }
    }

    code: {
      worker: {
        name: "worker"
        image: {
          hub: { name: "", secret: "" }
          tag: "kumoripublic/examples-sharded-volumes-worker:v1.0.7"
        }
        mapping: {
          filesystem: {
            "/usr/src/app/config/config.json": {
              format: "json"
              data: value: {
                shardSize: config.parameter.shardSize
                store: config.parameter.store
                serverPort: srv.server.bddserver.port
              }
            },
            // To be resolved in service model: key are path, so if the path
            // is defined in a variable, it results in an ugly syntax.
            "\(description.config.parameter.store)": {
              volume: "store"
            }
          }
          env: {
            CONFIG_FILE: value: "/usr/src/app/config/config.json"
          }
        }
        size: {
          memory: { size: 200, unit: "M" }
          mincpu: 200
          cpu: { size: 250, unit: "m" }
        }
      }
    }
  }
}
