package component

#Artifact: {
  ref: name:  "frontend"

  description: {

    srv: {
      server: {
        restapi: { protocol: "http", port: 8080 }
      }
      client: {
        bddclient: { protocol: "http" }
      }
    }

    config: {
      parameter: shardSize: int
      resource: {}
    }

    size: {
      bandwidth: { size: 10, unit: "M" }
    }

    probe: frontend: {
      liveness: {
        protocol: http : { port: srv.server.restapi.port, path: "/health" }
        startupGraceWindow: { unit: "ms", duration: 300000, probe: true }
        frequency: "medium"
        timeout: 30000  // msec
      }
      readiness: {
        protocol: http : { port: srv.server.restapi.port, path: "/health" }
        frequency: "medium"
        timeout: 30000  // msec
      }
    }

    code: {
      frontend: {
        name: "frontend"
        image: {
          hub: { name: "", secret: "" }
          tag: "kumoripublic/examples-sharded-volumes-frontend:v1.0.7"
        }
        mapping: {
          filesystem: {
            "/usr/src/app/config/config.json": {
              data: value: {
                shardSize: config.parameter.shardSize
                endpoint: "0.bddclient"
                serverPort: srv.server.restapi.port
              }
              format: "json"
            }
          }
          env: {
            CONFIG_FILE: value: "/usr/src/app/config/config.json"
          }
        }
        size: {
          memory: { size: 200, unit: "M" }
          mincpu: 200
          cpu: { size: 250, unit: "m" }
        }
      }
    }
  }
}
