#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
INBOUNDNAME="shardvolumesinb"
DEPLOYNAME="shardvolumesdep"
DOMAIN="shardvolumesdomain"
VOLUME="myshardedvolume"
SERVICEURL="shardvolumes-${CLUSTERNAME}.${REFERENCEDOMAIN}"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"

# Change if special binaries want to be used
KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-volume')
  ${KAM_CTL_CMD} register volume $VOLUME \
    --items 5 \
    --size 100Mi \
    --type persistent
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

'deploy-inbound')
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $CLUSTERCERT
  ;;

# To check if our module is correct (from the point of view of the CUE syntax and
# the Kumori service model), we can use "kam process" to enerate the JSON
# representation
'dry-run')
  ${KAM_CMD} process deployment -t ./manifests
  ;;


'deploy-service')
  ${KAM_CMD} service deploy -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "Volumes example service" \
    --wait 5m
  ;;

'link')
  ${KAM_CTL_CMD} link $DEPLOYNAME:restapi $INBOUNDNAME:inbound
  ;;

'deploy-all')
  $0 create-volume
  $0 create-domain
  $0 deploy-inbound
  $0 deploy-service
  $0 link
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  echo
  ${KAM_CTL_CMD} describe volume $VOLUME
  ;;

# Test the volumes service.
# ASSUMES SHARDSIZE = 3
# key = aaa => shard 0
# key = bbb => shard 0
# key = aab => shard 1
'test')
  echo -----------------------------------------
  echo Getting not existing aaa,bbb,aab keys; echo
  curl -X GET https://${SERVICEURL}/keys/aaa; echo
  curl -X GET https://${SERVICEURL}/keys/bbb; echo
  curl -X GET https://${SERVICEURL}/keys/aab; echo
  echo -----------------------------------------
  echo Creating aaa,bbb,aab key; echo
  curl -X POST https://${SERVICEURL}/keys/aaa/value-aaa
  curl -X POST https://${SERVICEURL}/keys/bbb/value-bbb
  curl -X POST https://${SERVICEURL}/keys/aab/value-aab
  echo -----------------------------------------
  echo Getting existing aaa,bbb,aab key; echo
  curl -X GET https://${SERVICEURL}/keys/aaa; echo
  curl -X GET https://${SERVICEURL}/keys/bbb; echo
  curl -X GET https://${SERVICEURL}/keys/aab; echo
  echo -----------------------------------------
  echo Deleting aaa,bbb,aab key; echo
  curl -X DELETE https://${SERVICEURL}/keys/aaa
  curl -X DELETE https://${SERVICEURL}/keys/bbb
  curl -X DELETE https://${SERVICEURL}/keys/aab
  echo -----------------------------------------
  echo Getting not existing aaa,bbb,aab key; echo
  curl -X GET https://${SERVICEURL}/keys/aaa; echo
  curl -X GET https://${SERVICEURL}/keys/bbb; echo
  curl -X GET https://${SERVICEURL}/keys/aab; echo
  echo -----------------------------------------
  ;;

'unlink')
  ${KAM_CTL_CMD} unlink $DEPLOYNAME:restapi $INBOUNDNAME:inbound
  ;;

'undeploy-service')
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --force --wait 5m
  ;;

'undeploy-inbound')
  ${KAM_CMD} service undeploy $INBOUNDNAME -- --wait 5m
  ;;

'delete-volume')
  ${KAM_CTL_CMD} unregister volume $VOLUME
  ;;

'delete-domain')
  ${KAM_CTL_CMD} unregister domain $DOMAIN
  ;;

'undeploy-all')
  $0 undeploy-service
  $0 undeploy-inbound
  $0 delete-volume
  $0 delete-domain
  ;;

# # When community images are not used:
# 'create-images-in-kv3')
#   cd ./code/frontend
#   docker rmi -f registry.gitlab.com/kumori/kv3/examples/sharded-volumes/frontend:v1.0.2
#   docker build . -t registry.gitlab.com/kumori/kv3/examples/sharded-volumes/frontend:v1.0.2
#   cd ../..

#   cd ./code/worker
#   docker rmi -f registry.gitlab.com/kumori/kv3/examples/sharded-volumes/worker:v1.0.2
#   docker build . -t registry.gitlab.com/kumori/kv3/examples/sharded-volumes/worker:v1.0.2
#   cd ../..

#   docker login registry.gitlab.com
#   docker push registry.gitlab.com/kumori/kv3/examples/sharded-volumes/frontend:v1.0.2
#   docker push registry.gitlab.com/kumori/kv3/examples/sharded-volumes/worker:v1.0.2
#   docker logout
#   ;;

*)
  echo "This script doesn't contain that command"
	;;

esac