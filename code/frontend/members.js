/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
const dns = require('dns')
const axios = require('axios')

const ID_KEY = '__ID__'
const UPDATE_PERIOD = 15000
const KUMORI_ROLE_SUBDOMAIN = process.env.KUMORI_ROLE_SUBDOMAIN

// Maintains a dictionary of members related to the config.endopoint (in this
// examples, config.endpoint = 0.bddclient).
//
// Dictionary structure:
//   keys: shard index,
//   value: member endpoint
//
// For example:
//   {
//     '0': '172-17-118-24.bddconnector000-0-kd-121212-e76d4142.kumori.svc.cluster.local:8080',
//     '1': '172-17-118-26.bddconnector000-0-kd-121212-e76d4142.kumori.svc.cluster.local:8080',
//     '2': '172-17-118-28.bddconnector000-0-kd-121212-e76d4142.kumori.svc.cluster.local:8080'
//   }
//
class Members {

  //
  // PUBLIC METHODS
  //


  // Config parameter example:
  // {
  //   shardSize: 3
  //   endpoint: 0.bddclient  (tag.channel)
  // }
  constructor(config) {
    this.config = config
    this.members = {}
    this.updateMembers()
    setInterval(() => this.updateMembers(), UPDATE_PERIOD)
  }

  // For health support
  allMembersReady() {
    let membersReadyCount = 0
    for (let [key, member] of Object.entries(this.members)) {
      if ( (member != undefined) && (member != null) ) {
        membersReadyCount++
      }
    }
    return (membersReadyCount == this.config.shardSize)
  }

  // For a key, calculates it shard number and returns the member for that shard
  getMemberFromKey(key) {
    return new Promise((resolve, reject) => {
      this._getShardFromKey(key)
      .then((shard) => {
        let member = this.members[shard]
        if (member == undefined) {
          reject(new Error(`Member for shard ${shard} is undefined`))
        } else {
          console.log('Members.getMemberFromKey: member =', member)
          resolve(member)
        }
      })
      .catch((err) => {
        console.log('Members.getMemberFromKey: error =', err.message)
        reject(err)
      })
    })
  }


  // Update the list of members
  // This method can be private, since it is not necessary to invoke it "from
  // outside". It is used from the constructor of this class, in a "setInterval"
  updateMembers() {
    return new Promise((resolve) => {
      console.log('Members.updateMembers')
      this.members = {}
      for (let i = 0; i < this.config.shardSize; i++) {
        this.members[`${i}`] = undefined
      }
      this._dnsResolveChannel(this.config.endpoint)
      .then((srvRecords) => {
        console.log('Members.updateMembers: resolved endpoint', this.config.endpoint)
        let promises = []
        for (let srvRecord of srvRecords) {
          promises.push(this._getMemberFromSrvRecord(srvRecord))
        }
        return Promise.allSettled(promises)
      })
      .then((results) => {
        for (let result of results) {
          if (result.status == 'fulfilled') {
            let memberInfo = result.value
            if (parseInt(memberInfo.shard) < this.config.shardSize) {
              this.members[memberInfo.shard] = memberInfo.endpoint
            }
          }
        }
        console.log('Members.updateMembers: members =', this.members)
        return this.members
      })
      .catch((err) => {
        console.log(`Members.updateMembers:  error = ${err.message}`)
        resolve()
      })
    })
  }


  //
  // PRIVATE METHODS
  //


  // This method must exists in the frontend component too
  _getShardFromKey(key) {
    return new Promise((resolve, reject) => {
      if (key == ID_KEY) {
        reject(new Error('The key __ID__ is reserved'))
      }
      let value = 0
      for (let i = 0; i < key.length; i++) {
        value = value + key.charCodeAt(i)
      }
      resolve(value%this.config.shardSize)
    })
  }


  // Returns a promise resolved with:
  //   [
  //     {
  //       name: '172-17-234-176.bddconnector000-0-kd-150837-8e29d7b8.kumori.svc.cluster.local',
  //       port: 8080,
  //       priority: 0,
  //       weight: 33
  //     },
  //     [...]
  //   ]
  //
  //
  // Problem when resolving domain names using NodeJS.
  //
  // We want resolve SRV records for the domain name "0.bddclient".
  // But, really, the domain name to be resolved is:
  //   0.bddclient.frontend000.kd-150837-8e29d7b8.kumori.dep.cluster.local
  // where the suffix "frontend000.kd-150837-8e29d7b8.kumori.dep.cluster.local"
  // is included by the operating system when, for example nslookup or dig
  // commands, because appears in etc/resolv.conv/search section.
  //
  // But:
  // - NodeJS/dns.lookup() doesn't resolve SRV records
  // - NodeJS/dns.resolve() doesn0t use the etc/resolv.conf file
  //
  // So, in this example, we add the suffix before use dns.resolve() command.
  // The suffix is derived from the instance name:
  // - Instance: kd-150837-8e29d7b8-frontend000-deployment-b77b44b58-7fsfh:
  // - Suffix: "."" + "frontend000" + "".kd-150837-8e29d7b8" + ".kumori.dep.cluster.local"
  //
  _dnsResolveChannel(endpoint) {
    return new Promise((resolve, reject) => {
      // endpoint: 0.bddclient
      // KUMORI_ROLE_SUBDOMAIN: frontend000.kd-103355-86971924.kumori.dep.cluster.local
      let domainName = endpoint + "." + KUMORI_ROLE_SUBDOMAIN
      dns.resolve(domainName, 'SRV', (err, srvRecords) => {
        if (err != undefined) {
          reject(err)
        } else {
          resolve(srvRecords)
        }
      })
    })
  }


  // Returns an object with the shardID and the endpoint of the member
  //
  // srvRecord example:
  // {
  //   name: "172-17-234-176.bddconnector000-0-kd-150837-8e29d7b8.kumori.svc.cluster.local",
  //   port: 8080,
  //   priority: 0,
  //   weight: 33
  // }
  //
  // Result example:
  // {
  //   shard: "0"
  //   endpoint: "172-17-234-176.bddconnector000-0-kd-150837-8e29d7b8.kumori.svc.cluster.local"
  // }
  _getMemberFromSrvRecord(srvRecord) {
    return new Promise((resolve, reject) => {
      let endpoint = srvRecord.name + ":" + srvRecord.port
      let url = `http://${endpoint}/keys/${ID_KEY}`
      axios.get(url)
      .then((res) => {
        if (res.status != 200) {
          console.log(`_getMemberFromSrvRecord(${url}) status: ${res.status}`)
          reject(new Error(`status: ${res.status}`))
        } else {
          resolve({
            shard: res.data,
            endpoint: endpoint
          })
        }
      })
      .catch((err) => {
        console.log(`_getMemberFromSrvRecord(${url}) error: ${err.message}`)
        reject(err)
      })
    })
  }
}


module.exports = Members