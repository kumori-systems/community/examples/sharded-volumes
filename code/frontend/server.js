/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

const fs = require('fs')
const express = require('express')
const axios = require('axios')
const Members = require('./members')

const HOST_NAME = '0.0.0.0'
const CONFIG_FILE = process.env.CONFIG_FILE

var store = null
var members = null
var applicationReady = false


readConfig = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(CONFIG_FILE, 'utf8', (err, jsonString) => {
      if (err) {
        reject(err)
        return
      }
      try {
        appConfig = JSON.parse(jsonString)
        resolve(appConfig)
      } catch (err) {
        reject(err)
      }
    })
  })
}


createExpressApp = () => {
  let app = express()

  app.get('/keys/:key', (req, res) => {
    key = req.params.key
    console.log(`GET ${key}`)
    members.getMemberFromKey(key)
    .then((member) => { return axios.get(`http://${member}/keys/${key}`) })
    .then((result) => { res.send(result.data) })
    .catch((err) => { res.status(503).send(err.isAxiosError ? err.response.data : err.message) })
  })

  app.post('/keys/:key/:value', (req, res) => {
    key = req.params.key
    value = req.params.value
    console.log(`POST ${key}=${value}`)
    members.getMemberFromKey(key)
    .then((member) => { return axios.post(`http://${member}/keys/${key}/${value}`) })
    .then((result) => { res.send(result.data) })
    .catch((err) => { res.status(503).send(err.isAxiosError ? err.response.data : err.message) })
  })

  app.delete('/keys/:key', (req, res) => {
    key = req.params.key
    console.log(`DELETE ${key}`)
    members.getMemberFromKey(key)
    .then((member) => { return axios.delete(`http://${member}/keys/${key}`) })
    .then((result) => { res.send(result.data) })
    .catch((err) => { res.status(503).send(err.isAxiosError ? err.response.data : err.message) })
  })

  app.get('/health', (req, res) => {
    if ( (applicationReady == true) &&
         (members != null) &&
         (members.allMembersReady() == true) ) {
      console.log('Health request OK', new Date())
      res.send('OK')
    } else {
      console.log('Health request failure', new Date())
      res.status(503).send('NotReady')
    }
  })

  app.get('/killme', (req, res) => {
    console.log('Kill me!!')
    process.exit()
  })

  app.use(function(req, res, next) {
    res.status(404).send('Not found')
  })

  return app
}

readConfig()
.then((cfg) => {
  config = cfg
  console.log(`Configuration: ${JSON.stringify(cfg)}`)
  members = new Members(config)
  app = createExpressApp()
  app.listen(config.serverPort, () => {
    console.log(`Server running at http://${HOST_NAME}:${config.serverPort}/`)
    applicationReady = true
  })
})
.catch((err) => {
  console.log(`Error initializating component: ${err.message}`)
})
