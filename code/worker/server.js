/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

const fs = require('fs')
const express = require('express')
const Store = require('./store')

const HOST_NAME = '0.0.0.0'
const CONFIG_FILE = process.env.CONFIG_FILE
const KUMORI_INSTANCE_ID = process.env.KUMORI_INSTANCE_ID

var store = null
var config = null
const ID_KEY = '__ID__'
var ID_VALUE = ''
var applicationReady = false


getIdFromInstance = () => {
  return KUMORI_INSTANCE_ID.slice(-1)
}


readConfig = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(CONFIG_FILE, 'utf8', (err, jsonString) => {
      if (err) {
        reject(err)
        return
      }
      try {
        appConfig = JSON.parse(jsonString)
        resolve(appConfig)
      } catch (err) {
        reject(err)
      }
    })
  })
}


initializeDatabase = () => {
  return new Promise((resolve, reject) => {
    store.get(ID_KEY)
    .then((result) => {
      console.log(`Key __ID__ already exists: ${result}`)
      ID_VALUE = result
      resolve()
    })
    .catch((err) => {
      console.log(`Key __ID__ doesnt exists: creating  [original error: ${err.message}]`)
      ID_VALUE = getIdFromInstance()
      store.add(ID_KEY, ID_VALUE)
      resolve()
    })
  })
}


// This method must exists in the frontend component too
getShardFromKey = (key) => {
  let value = 0
  for (let i = 0; i < key.length; i++) {
    value = value + key.charCodeAt(i)
  }
  return value%config.shardSize
}


checkShardKey = (key) => {
  if (key == ID_KEY) {
    return Promise.resolve()
  }
  shardId = getShardFromKey(key)
  if (shardId != ID_VALUE) {
    err = new Error(`Unexpected shard for key ${key}. Calculated ${shardId}, expected ${ID_VALUE}`)
    return Promise.reject(err)
  }
  return Promise.resolve()
}


createExpressApp = () => {

  let app = express()

  // Retrieve a key
  app.get('/keys/:key', (req, res) => {
    key = req.params.key
    console.log(`GET ${key}`)
    checkShardKey(key)
    .then(() => { return store.get(key) })
    .then((result) => { res.send(result) })
    .catch((err) =>   { res.status(503).send(err.message) })
  })

  // Create a key
  app.post('/keys/:key/:value', (req, res) => {
    key = req.params.key
    value = req.params.value
    console.log(`POST ${key}=${value}`)
    checkShardKey(key)
    .then(() => { return store.add(key, value) })
    .then((result) => { res.send(result) })
    .catch((err) =>   { res.status(503).send(err.message) })
  })

  // Delete a key
  app.delete('/keys/:key', (req, res) => {
    key = req.params.key
    console.log(`DELETE ${key}`)
    checkShardKey(key)
    .then(() => { return store.delete(key) })
    .then((result) => { res.send(result) })
    .catch((err) =>   { res.status(503).send(err.message) })
  })

  app.get('/health', (req, res) => {
    if (applicationReady == true) {
      console.log('Health request OK', new Date())
      res.send('OK')
    } else {
      console.log('Health request failure', new Date())
      res.status(503).send('NotReady')
    }
  })

  app.use(function(req, res, next) {
    res.status(404).send('Not found')
  })

  return app
}


readConfig()
.then((cfg) => {
  config = cfg
  console.log(`Configuration: ${JSON.stringify(cfg)}`)
  if ( (config.store != null) && (config.store != undefined) ) {
    store = new Store('store', config.store)
    return store.init()
  } else {
    throw new Error('Store not available')
  }
})
.then(() => {
  return initializeDatabase()
})
.then(() => {
  app = createExpressApp()
  app.listen(config.serverPort, () => {
    console.log(`Server running at http://${HOST_NAME}:${config.serverPort}/`)
    applicationReady = true
  })
})
.catch((err) => {
  console.log(`Error initializating component: ${err.message}`)
})